<?php

/*
*  CONKRETEMOS SAS
*  Licencia de cabeceras para el proyecto CONKRETEMOS SAS
*  author:  ing Jaime Diaz G.
*  2016  COMPANY
*/ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="es">
<head>
         <base href="<?php print(URL_SINGLE_APPLICATION) ?>">
	<!--<meta charset="UTF-8">-->
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width= device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1">
	<title><?php print($this->title); ?></title>
         <link href="./public/styles/lib/normalize.css" rel="stylesheet">
   
	<link href="./public/styles/concretemosSAS-main-styles.css" rel="stylesheet">        
             
         <!--CARGA DE LIBRERIAS CSS :: USABILIDAD-->
         <link href="./public/styles/lib/alertify.core.css" rel="stylesheet" type="text/css">
         <link href="./public/styles/lib/alertify.default.css" rel="stylesheet" type="text/css">
         <link href="./public/css/styles/alertify.bootstrap.css" rel="stylesheet" type="text/css">
             
             <!--scripts LIBRERIAS JS :: USABILIDAD-->
         <script src="<?php print(URL_SINGLE_APPLICATION); ?>public/js/lib/jquery-1.12.1.js"></script>
         <script src="<?php print(URL_SINGLE_APPLICATION); ?>public/plugins/parallax-js/parallax.min.js"></script>

</head>




